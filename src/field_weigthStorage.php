<?php

    namespace Drupal\field_weight;
    
    class field_weightStorage {
        static function merge($nid,$vid,$type){
            db_merge('field_weight')
            ->key(array('vid' => $vid))
            ->fields(array(
                'nid' => $nid,
                'vid' => $vid,
                'type' => $type,
                'field_weights' => serialize($field_weights),
                ))
                ->execute();
        }
        static function delete($id){
            db_delete('field_weight')
            ->condition('nid', $id)
            ->execute();
        }
    }