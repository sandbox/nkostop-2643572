<?php

    namespace Drupal\field_weight;
    
    use Drupal\Core\Form\FormInterface;
    
    class overviewForm implements FormInterface {
        
        function getFormID() { return 'field_weight_display_overview_form'; }
        
        function buildForm(array $form, array &$form_state) {
            $form['name'] = array(
                '#type' => 'textfield',
                '#title' => t('Name'),
            );
            $form['message'] = array(
                '#type' => 'textarea',
                '#title' => t('Message'),
            );
            $form['actions'] = array('#type' => 'actions');
            $form['actions']['submit'] = array(
                '#type' => 'submit',
                '#value' => t('Add'),
            );
            return $form;
        }
        
        function validateForm(array &$form, array &$form_state) {
            /*Nothing to validate on this form*/
        }
        
        function submitForm(array &$form, array &$form_state) {
            watchdog('bd_contact', 'test form has been submitted.');
            drupal_set_message(t('Your message has been submitted'));
            //$form_state['redirect'] = 'admin/content/';
            return;
        }
    }